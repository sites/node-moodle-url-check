"use strict";

var request = require("request");
var assert = require("assert");
var cheerio = require("cheerio");

var URLROOT = "https://moodle.org";

describe("accessing moodle.org/plugins/", function() {
    this.timeout(10000);

    describe("plugin page via view.php", function() {
        var pluginurls = [
            URLROOT + "/plugins/view.php?id=127",
            URLROOT + "/plugins/view.php?plugin=block_course_contents"
        ];

        pluginurls.forEach(function(pluginurl) {
            it("should work for URL " + pluginurl, function(done) {
                request(pluginurl, function (error, response, body) {
                    var $ = cheerio.load(body);

                    assert.ifError(error);
                    assert.equal(response.statusCode, 200);
                    assert($(".plugin-heading .frankenstyle").text() == "block_course_contents");
                    done();
                });
            });
        });
    });

    describe("report pages", function() {
        var reporturls = [
            URLROOT + "/plugins/report",
            URLROOT + "/plugins/report/",
            URLROOT + "/plugins/report/?report=approved_plugins",
            URLROOT + "/plugins/report/index.php?report=approved_plugins"
        ];

        reporturls.forEach(function(reporturl) {
            it("should work for URL " + reporturl, function(done) {
                request(reporturl, function (error, response, body) {
                    var $ = cheerio.load(body);

                    assert.ifError(error);
                    assert.equal(response.statusCode, 200);
                    assert($("body").attr("id") == "page-plugins-report-index");
                    done();
                });
            });
        });
    });

    describe("plugin page via /plugins/view/xxx_yyy", function() {
        var samples = ["mod_stampcoll", "block_course_contents", "qformat_hotpot", "report_overviewstats"];

        samples.forEach(function(sample) {
            var pluginurl = URLROOT + "/plugins/view/" + sample;
            var redirectedurl = URLROOT + "/plugins/" + sample;

            it("should work for URL " + pluginurl, function(done) {
                request(pluginurl, function (error, response, body) {
                    var $ = cheerio.load(body);

                    assert.ifError(error);
                    assert.equal(response.statusCode, 200);
                    assert($(".plugin-heading .frankenstyle").text() == sample);
                    assert.equal(response.request.uri.href, redirectedurl);
                    done();
                });
            });
        });
    });

    describe("plugin page via /plugins/xxx_yyy", function() {
        var samples = ["mod_stampcoll", "block_course_contents", "qformat_hotpot", "report_overviewstats"];

        samples.forEach(function(sample) {
            var pluginurl = URLROOT + "/plugins/" + sample;

            it("should work for URL " + pluginurl, function(done) {
                request(pluginurl, function (error, response, body) {
                    var $ = cheerio.load(body);

                    assert.ifError(error);
                    assert.equal(response.statusCode, 200);
                    assert($(".plugin-heading .frankenstyle").text() == sample);
                    done();
                });
            });
        });
    });
});
